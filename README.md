lua-ConciseSerialization : CBOR RFC8949
=======================================

Introduction
------------

The [Concise Binary Object Representation](https://cbor.io/)
([RFC 8949](https://tools.ietf.org/html/rfc8949)) is a data format
whose design goals include the possibility of extremely small code size,
fairly small message size, and extensibility without the need for version negotiation.

It's a pure Lua implementation, without dependency.

Links
-----

The homepage is at <https://fperrad.frama.io/lua-ConciseSerialization/>,
and the sources are hosted at <https://framagit.org/fperrad/lua-ConciseSerialization/>.

Copyright and License
---------------------

Copyright (c) 2016-2023 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.
