#! /usr/bin/lua

require 'Test.Assertion'

plan(35)

local c = require 'CBOR'

equals( c.decode(c.encode(true)), true, "true" )
equals( c.decode(c.encode(false)), false, "false" )

equals( c.decode(c.SIMPLE(20)), false, "false" )
equals( c.decode(c.SIMPLE(21)), true, "true" )
equals( c.decode(c.SIMPLE(22)), nil, "null" )
equals( c.decode(c.SIMPLE(23)), nil, "undef" )
equals( c.decode(c.SIMPLE(2)), 2, "simple 2" )
equals( c.decode(c.SIMPLE(42)), 42, "simple 42" )

c.set_nil'null'
equals( c.decode(c.encode(nil)), nil, "nil" )

c.set_float'single'
local nan = c.decode(c.encode(0/0))
is_number( nan, "nan" )
truthy( nan ~= nan )
equals( c.decode(c.encode(3.140625)), 3.140625, "3.140625" )
equals( c.decode(c.encode(-2)), -2, "neg -2" )
equals( c.decode(c.encode(42)), 42, "pos 42" )

c.set_float'half'
nan = c.decode(c.encode(0/0))
is_number( nan, "nan" )
truthy( nan ~= nan )
equals( c.decode(c.encode(3.125)), 3.125, "3.125" )
equals( c.decode(c.encode(-2)), -2, "neg -2" )
equals( c.decode(c.encode(42)), 42, "pos 42" )

c.set_string'text_string'
local s = string.rep('x', 2^3)
equals( c.decode(c.encode(s)), s, "#s 2^3" )
s = string.rep('x', 2^7)
equals( c.decode(c.encode(s)), s, "#s 2^7" )
s = string.rep('x', 2^11)
equals( c.decode(c.encode(s)), s, "#s 2^11" )
s = string.rep('x', 2^19)
equals( c.decode(c.encode(s)), s, "#s 2^19" )

c.set_string'byte_string'
s = string.rep('x', 2^3)
equals( c.decode(c.encode(s)), s, "#s 2^3" )
s = string.rep('x', 2^5)
equals( c.decode(c.encode(s)), s, "#s 2^5" )
s = string.rep('x', 2^11)
equals( c.decode(c.encode(s)), s, "#s 2^11" )
s = string.rep('x', 2^19)
equals( c.decode(c.encode(s)), s, "#s 2^19" )

local t = { string.rep('x', 2^3):byte(1, -1) }
same( c.decode(c.encode(t)), t, "#t 2^3" )
t = { string.rep('x', 2^9):byte(1, -1) }
same( c.decode(c.encode(t)), t, "#t 2^9" )
while #t < 2^17 do t[#t+1] = 'x' end
same( c.decode(c.encode(t)), t, "#t 2^17" )

local h = {}
for i = 1, 2^3 do h[10*i] = 'x' end
same( c.decode(c.encode(h)), h, "#h 2^3" )
h = {}
for i = 1, 2^9 do h[10*i] = 'x' end
same( c.decode(c.encode(h)), h, "#h 2^9" )
for i = 1, 2^17 do h[10*i] = 'x' end
same( c.decode(c.encode(h)), h, "#h 2^17" )

if utf8 then
    c.set_string'check_utf8'
    equals( c.encode("\x4F"):byte(), c.TEXT_STRING(1):byte(), "text" )
    equals( c.encode("\xFF"):byte(), c.BYTE_STRING(1):byte(), "byte" )
else
    skip("no utf8", 2)
end
