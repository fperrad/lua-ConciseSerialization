#! /usr/bin/lua

require 'Test.Assertion'

plan(8)

if not require_ok 'CBOR' then
    BAIL_OUT "no lib"
end

local m = require 'CBOR'
is_table( m )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'ConciseSerialization', "_DESCRIPTION" )
matches( m._VERSION, '^%d%.%d%.%d$', "_VERSION" )

is_table( m.coders, "table coders" )
is_function( m.decode_cursor, "function decode_cursor" )
is_string( m.MAGIC, "CBOR MAGIC" )

if _VERSION >= 'Lua 5.3' then
    diag "full 64bits with Lua >= 5.3"
end
