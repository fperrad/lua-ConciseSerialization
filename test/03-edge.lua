#! /usr/bin/lua

require 'Test.Assertion'

plan(50)

local c = require 'CBOR'

equals( c.TAG(55799), c.MAGIC, "self-describe CBOR" )

local cbor = c.encode({})
equals( c.decode(c.TAG(24) .. cbor), cbor, "encoded CBOR data item" )
equals( c.decode(c.TAG(42) .. c.encode'text'), 'text' )
equals( c.decode(c.TAG(42) .. c.TAG(42) .. c.encode'text'), 'text' )

equals( c.decode(c.encode(1.0/0.0)), 1.0/0.0, "inf" )

equals( c.decode(c.encode(-1.0/0.0)), -1.0/0.0, "-inf" )

local nan = c.decode(c.encode(0.0/0.0))
is_number( nan, "nan" )
truthy( nan ~= nan )

equals( c.encode{}:byte(), c.ARRAY(0):byte(), "empty table as array" )

local t = setmetatable( { 'a', 'b', 'c' }, { __index = { [4] = 'd' } } )
equals( t[4], 'd' )
t = c.decode(c.encode(t))
equals( t[2], 'b' )
equals( t[4], nil, "don't follow metatable" )

t = setmetatable( { a = 1, b = 2, c = 3 }, { __index = { d = 4 } } )
equals( t.d, 4 )
t = c.decode(c.encode(t))
equals( t.b, 2 )
equals( t.d, nil, "don't follow metatable" )

t = { 10, 20, nil, 40 }
c.set_array'without_hole'
equals( c.encode(t):byte(), c.MAP(3):byte(), "array with hole as map" )
same( c.decode(c.encode(t)), t )
c.set_array'with_hole'
equals( c.encode(t):byte(), c.ARRAY(4):byte(), "array with hole as array" )
same( c.decode(c.encode(t)), t )
c.set_array'always_as_map'
equals( c.encode(t):byte(), c.MAP(3):byte(), "always_as_map" )
same( c.decode(c.encode(t)), t )

t = {}
c.set_array'without_hole'
equals( c.encode(t):byte(), c.ARRAY(0):byte(), "empty table as array" )
c.set_array'with_hole'
equals( c.encode(t):byte(), c.ARRAY(0):byte(), "empty table as array" )
c.set_array'always_as_map'
equals( c.encode(t):byte(), c.MAP(0):byte(), "empty table as map" )

c.set_float'half'
equals( c.encode(65536.1), c.encode(1.0/0.0), "half 65536.1")
equals( c.encode(66666.6), c.encode(1.0/0.0), "inf (downcast double -> half)")
equals( c.encode(-66666.6), c.encode(-1.0/0.0), "-inf (downcast double -> half)")
equals( c.decode(c.encode(66666.6)), 1.0/0.0, "inf (downcast double -> half)")
equals( c.decode(c.encode(-66666.6)), -1.0/0.0, "-inf (downcast double -> half)")
equals( c.decode(c.encode(7e-6)), 0.0, "epsilon (downcast double -> half)")
equals( c.decode(c.encode(-7e-6)), -0.0, "-epsilon (downcast double -> half)")

c.set_float'single'
equals( c.encode(3.402824e+38), c.encode(1.0/0.0), "float 3.402824e+38")
equals( c.encode(7e42), c.encode(1.0/0.0), "inf (downcast double -> float)")
equals( c.encode(-7e42), c.encode(-1.0/0.0), "-inf (downcast double -> float)")
equals( c.decode(c.encode(7e42)), 1.0/0.0, "inf (downcast double -> float)")
equals( c.decode(c.encode(-7e42)), -1.0/0.0, "-inf (downcast double -> float)")
equals( c.decode(c.encode(7e-46)), 0.0, "epsilon (downcast double -> float)")
equals( c.decode(c.encode(-7e-46)), -0.0, "-epsilon (downcast double -> float)")

c.set_float'double'
if c.long_double then
    equals( c.encode(7e400), c.encode(1.0/0.0), "inf (downcast long double -> double)")
    equals( c.encode(-7e400), c.encode(-1.0/0.0), "-inf (downcast long double -> double)")
    equals( c.decode(c.encode(7e400)), 1.0/0.0, "inf (downcast long double -> double)")
    equals( c.decode(c.encode(-7e400)), -1.0/0.0, "-inf (downcast long double -> double)")
    equals( c.decode(c.encode(7e-400)), 0.0, "epsilon (downcast long double -> double)")
    equals( c.decode(c.encode(-7e-400)), -0.0, "-epsilon (downcast long double -> double)")
else
    skip("no long double", 6)
end

if _VERSION >= 'Lua 5.3' then
    equals( c.decode(c.encode(0xFFFFFFFFFFFFFFFF)), 0xFFFFFFFFFFFFFFFF, "64 bits")
else
    skip("53 bits", 1)
end

cbor = string.char(0xA2, 0xF7, 0x01, 0x42, 0x69, 0x64, 0x02)
t = c.decode(cbor)
equals( t.id, 2, "decode map with nil as table index" )

c.sentinel = {}
t = c.decode(cbor)
equals( t[c.sentinel], 1, "decode using a sentinel for nil as table index" )
equals( t.id, 2 )

cbor = string.char(0xA2, 0x42, 0x69, 0x64, 0x01, 0x42, 0x69, 0x64, 0x02)
error_matches(function ()
               c.decode(cbor)
        end,
        "duplicated keys" )

c.strict = false
not_errors(function ()
             c.decode(cbor)
        end,
        "duplicated keys" )
