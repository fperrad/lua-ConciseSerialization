
# lua-ConciseSerialization

---

## Overview

The [Concise Binary Object Representation](https://cbor.io/)
([RFC 8949](https://tools.ietf.org/html/rfc8949)) is a data format
whose design goals include the possibility of extremely small code size,
fairly small message size, and extensibility without the need for version negotiation.

It's a pure Lua implementation, without dependency.

In order to stay a small module, there is no extension included.
But the API allows to register the standard extension (published
by [IANA](https://www.iana.org/assignments/cbor-tags/cbor-tags.xhtml)
) or your own.

## References

The homepage of CBOR is available at <https://cbor.io/>.

CBOR is specified in the [RFC-8949](https://tools.ietf.org/html/rfc8949).

## Status

lua-ConciseSerialization is in beta stage.

It's developed for Lua 5.1, 5.2, 5.3 & 5.4. There are 2 variants:

- one compatible with all interpreters since Lua 5.1
- another which uses the Lua 5.3 features


## Download

The sources are hosted on [Framagit](https://framagit.org/fperrad/lua-ConciseSerialization).

The Teal type definition of this library is available
[here](https://github.com/teal-language/teal-types/blob/master/types/lua-conciseserialization/CBOR.d.tl).

## Installation

lua-ConciseSerialization is available via LuaRocks:

```sh
luarocks install lua-conciseserialization
# luarocks install lua-conciseserialization-lua53
```

lua-ConciseSerialization is available via opm:

```sh
opm get fperrad/lua-cbor
```

or manually, with:

```sh
make install LUAVER=5.2
```

## Test

The test suite requires the module
[lua-TestAssertion](https://fperrad.frama.io/lua-testassertion/).

```sh
make test
```

## Copyright and License

Copyright &copy; 2016-2023 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license,
like Lua itself.
